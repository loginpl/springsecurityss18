package com.haladyj.SS18RS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss18RsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ss18RsApplication.class, args);
	}

}
