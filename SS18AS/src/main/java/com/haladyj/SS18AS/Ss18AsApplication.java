package com.haladyj.SS18AS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss18AsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ss18AsApplication.class, args);
	}

}
